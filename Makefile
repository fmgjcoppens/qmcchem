include make.config


default_target: all

.phony: clean always

clean:
	./scripts/clean.sh

# put all files of PROPERTIES here
# --------------------------------

ezfio_config/properties.config ocaml/Property.ml: scripts/create_properties_python.py src/PROPERTIES/properties.irp.f src/PROPERTIES/properties_energy.irp.f src/PROPERTIES/properties_general.irp.f src/irpf90.make
	bash -c "source qmcchemrc ; exec python2 ./scripts/create_properties_ezfio.py"

# EZFIO 
# -----

build lib/libezfio.a lib/libezfio_irp.a EZFIO/lib/libezfio.a EZFIO/lib/libezfio_irp.a EZFIO/Ocaml/ezfio.ml EZFIO/Python/ezfio.py: ezfio_config/qmc.config ezfio_config/properties.config make.config scripts/create_properties_ezfio.py src/tags src/irpf90_entities src/irpf90.make src/irpf90.make
	./scripts/compile_ezfio.sh


# Fortran executables
# -------------------      


always: /dev/null

src/tags src/irpf90_entities src/irpf90.make: make.config always
	./scripts/compile_irpf90.sh

src/MAIN/qmc src/MAIN/vmc_test src/MAIN/qmc_create_walkers src/MAIN/qmcchem_info: lib/libezfio.a lib/libezfio_irp.a src/tags src/irpf90_entities src/irpf90.make src/irpf90.make
	./scripts/compile_src.sh

# OCaml
# -----

ocaml/qmcchem: EZFIO/Ocaml/ezfio.ml $(wildcard ocaml/*.ml)
	./scripts/compile_ocaml.sh

# Archive
# -------

qmcchem.tar.gz: all
	git archive --format tar.gz HEAD --prefix "QmcChem/" -7 -o qmcchem.tar.gz


# Binaries
# --------

bin/qmc: src/MAIN/qmc
	cp src/MAIN/qmc bin/

bin/qmcchem_info: src/MAIN/qmcchem_info
	cp src/MAIN/qmcchem_info bin/

bin/qmc_create_walkers: src/MAIN/qmc_create_walkers
	cp src/MAIN/qmc_create_walkers bin/

bin/qmcchem: ocaml/qmcchem
	cp ocaml/qmcchem bin/

all: bin/qmc bin/qmcchem_info bin/qmc_create_walkers bin/qmcchem 


